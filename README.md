# Ruby on Rails Toy app

![Screenshot1](screenshots/users.png)

---

![Screenshot2](screenshots/microposts.png)

You can find this app at https://railstoy-app.herokuapp.com/

## Installation

Make sure you have postgresql installed:

```bash
sudo apt-get update
sudo apt-get install postgresql postgresql-contrib
```

Make sure you have the `libpq` library in your system

```bash
sudo apt install libpq-dev
```

Install all gems

```bash
bundle install
```

## Running the tests

Make sure all the migrations have run:

```bash
bundle exec bin/rake db:migrate RAILS_ENV=test
```

And then

```bash
bundle exec rake
```


## Running the app

Make sure all the migrations have run:

```bash
bundle exec bin/rake db:migrate RAILS_ENV=development
bundle exec rake db:setup
```

And then

```bash
bundle exec rails s
```

Then go to <http://localhost:3000/>



## Installing the Heroku cli


You can install the heroku cli as a gem, but it is better to [install it in your system](https://devcenter.heroku.com/articles/heroku-cli) so that it is available to you for any language and not only in Ruby.


## Pushing to Heroku

Pushing to heroku will deploy the application.

Make sure you have an account in Heroku and log in:

```bash
heroku login
> Enter your Heroku credentials:
> Email: user@example.com
> Password: ***********
> Logged in as user@example.com
```

Create the app if it doesn't exist yet

```bash
heroku create
```

Make sure that the app is in your remotes

```bash
heroku git:remote -a yourapp
```

Then push

```bash
git push heroku master
```

# Random data

If you want to generate random data to fill the db:

* Go to <http://www.generatedata.com/>
* Fill in the fields as name and email.
* For name, select a Data Type of names and in options write `Name Surname`.
* For email, select "Email".
* Down in "Export types" click on the SQL tab, "database table name" is `users`, "database type" is SQLite and unckeck all the checkboxes.
* Check the "New window/tab" radio button and click on Generate and copy all of it.
* Open a sqlite session with `sqlite3 development.sqlite3` and paste it in the prompt.

You should now have new records. Enjoy!

